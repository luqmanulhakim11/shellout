#include <shell.h>

// Main
int main(int argc, char **argv) { main_loop(); }

/* Functions */

void main_loop(void)
{
  // char *path = getenv("PATH");
  char *path = malloc(strlen(getenv("PATH")) * sizeof(char) + 1);
  strcpy(path, getenv("PATH"));
  char **paths = tokenize(path, ":");

  while (1) {
    char *prog = NULL;
    char wd[PATH_MAX];
    getcwd(wd, sizeof(wd));
    printf("shellout (%s) -> ", wd);
    char *line = shell_read();
    char **tokens = NULL;
    // printf("%s", line); [> DEBUG please remove later <]
    if (line != NULL) {
      tokens = tokenize(line, " \n\t\r\a");
    }
    if (*tokens != NULL) /* Checks the first token for emptyness */
      prog = findprog(tokens[0], paths);
    if (prog != NULL) {
      shell_execute(prog, tokens);
    }

    free(prog);
    free(tokens);
    if (strcmp(line, "exit") == 0) {
      free(line);
      break;
    }
    if (line != NULL)
      free(line);
  }
  free(path);
  free(paths);
}

// Read lines from stdin and return them;
char *shell_read()
{
  char *line = NULL;
  // Getline will allocate the buffer, so use ssize_t = 0
  size_t len = 0;
  if (getline(&line, &len, stdin) == -1) {
    // Check for EOF
    if (feof(stdin)) {
      // Exit gracefully if EOF or Ctrl+D
      exit(EXIT_SUCCESS);
    } else {
      // Exit with failure
      perror("readline");
      exit(EXIT_FAILURE);
    }
  }

  return line;
}

char **tokenize(char *line, char *delim)
{
  int bufsizori = 32, bufsizcur = bufsizori, position;
  char **tokens = malloc(bufsizcur * sizeof(char *));
  char *token = NULL;

  if (tokens == NULL) {
    fprintf(stderr, "tokenize: error, unable to allocate memory");
    exit(EXIT_FAILURE);
  }

  position = 0;
  token = strtok(line, delim);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    if (position >= bufsizcur) {
      bufsizcur += bufsizori;
      tokens = realloc(tokens, bufsizcur * sizeof(char *));
      if (tokens == NULL) {
        fprintf(stderr, "tokenize: error, unable to allocate memory");
        exit(EXIT_FAILURE);
      }
    }

    token = strtok(NULL, delim);
  }

  free(token);
  tokens[position] = NULL;
  return tokens;
}

char *findprog(char *name, char **paths)
{
  int pos = 0;
  char *ret = NULL;
  int m = 1;

  // Check for shell implementations
  if (strcmp("cd", name) == 0) {
    ret = malloc((strlen(name) + 1) * sizeof(char));
    strcpy(ret, name);
    return ret;
  }
  //
  while (paths[pos] != NULL && m == 1) {
    char *dir = paths[pos];

    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;

    if ((dp = opendir(dir)) != NULL) {
      while ((entry = readdir(dp)) != NULL && m == 1) {
        lstat(entry->d_name, &statbuf);

        if (strcmp(name, entry->d_name) == 0) {

          ret = malloc((strlen(dir) + 2 + strlen(entry->d_name)) * sizeof(char));
          strcpy(ret, dir);
          strcat(ret, "/");
          strcat(ret, entry->d_name);
          m = 0;
        }
      }
    }

    pos++;
    closedir(dp);
  }
  return ret;
}

void shell_execute(char *name, char **args)
{
  int i = 0;
  long sum;
  pid_t pid;
  int status, ret;
  char **myenv = __environ;

  // Check for shell implementations
  if (strcmp(name, "cd") == 0) {
    chdir(args[1]);
    return;
  }

  pid = fork();
  if (pid == 0) {
    // Child
    execve(name, args, myenv);
  }
  if ((ret = waitpid(pid, &status, 0)) == -1) {
    fprintf(stderr, "%s:error\n", name);
    exit(EXIT_FAILURE);
  }
  if (ret == pid) {
  }
}
