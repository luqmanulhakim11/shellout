#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

void main_loop(void);
char *shell_read();
void shell_execute(char *name, char **args);
char **tokenize(char *line, char *delim);
char *findprog(char *name, char **paths);
